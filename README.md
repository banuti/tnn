# Tiny Neural Networks - TNN

Tiny neural networks (TNN) are a convenient, simple, accurate, and computationally efficient way to store data. TNN combine the accuracy of look-up-tables with the negligible memory footprint of polynomial fits.

A particularly suitable application is computational fluid dynamics (CFD), where TNN may replace JANAF tables (for ideal gases), or real fluid equations of state for high-pressure simulations.
